import * as dynamoDbLib from "../../libs/dynamodb-lib";
import { success, failure } from "../../libs/response-lib";

export async function main(event, context, callback) {
    // Request body is passed in as a JSON encoded string in 'event.body'
    const data = JSON.parse(event.body);
    const params = {
      TableName: process.env.casetrackerTable,
        // 'KeyConditionExpression' defines the condition for the query
        // - 'userId = :userId': only return items with matching 'userId'
        //   partition key
        // 'ExpressionAttributeValues' defines the value in the condition
        // - ':userId': defines 'userId' to be Identity Pool identity id
        //   of the authenticated user
        Key: {
        userId: event.requestContext.identity.cognitoIdentityId,
        caseId: event.pathParameters.id
        },
        // 'UpdateExpression' defines the attributes to be updated
        // 'ExpressionAttributeValues' defines the value in the update expression
        UpdateExpression: "SET caseState = :caseState, correspondances = :correspondances",
        ExpressionAttributeValues: {
          ":caseState": data.caseState || "PMA",
          ":correspondances": data.correspondances || 1
        },
        // 'ReturnValues' specifies if and how to return the item's attributes,
        // where ALL_NEW returns all attributes of the item after the update; you
        // can inspect 'result' below to see how it works with different settings
        ReturnValues: "ALL_NEW"

    };
  
    try {
        const result = await dynamoDbLib.call("update", params);
        return success({status: true});
      } catch (e) {
        return failure({ status: false });
      }
  }