import { success, failure } from "../../libs/response-lib";
var request = require('request-promise');

export async function main(event, context) {
  var options = {
        uri: "https://api.root.co.za/v1/transactions",
        method: "GET",
        auth: {
          user: process.env.rootSecretKey
        },
        json: true
    }

    let transaction = {
      amount: 0,
      type: "",
      createdAt: "",
      merchant: ""
    }

    let transactions = []

    try {

      var result = await request(options);
      console.log("result:   ", result)
      result.forEach(element => {
        
        transactions.push(
          {
          amount : element.amount,
          type : element.type,
          createdAt : element.created_at,
          merchant : element.merchant
          }
        )
      });



      return success(transactions);

  } catch (e) {
    return failure({ message: e.message });
  }
}


