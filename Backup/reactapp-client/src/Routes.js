import React from "react";
import { Route, Switch } from "react-router-dom";

import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import Login from "./containers/Login"
import Signup from "./containers/Signup"
import Notes from "./containers/notes/Notes"
import NewNote from "./containers/notes/NewNote";
import EditNote from "./containers/notes/EditNote"
import DashStats from "./containers/dash/Stats"
import Balance from "./containers/finance/Balance"
import NewBalance from "./containers/finance/NewBalance"
import EditBalance from "./containers/finance/EditBalance"
import Card from "./containers/card/Card"
import AppliedRoute from "./components/AppliedRoute"
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import UnauthenticatedRoute from "./components/UnauthenticatedRoute";


export default ({ childProps }) =>
  <Switch>        
    <AppliedRoute path="/" exact component={Home} props={childProps} />
    <UnauthenticatedRoute path="/login" exact component={Login} props={childProps} />
    <UnauthenticatedRoute path="/signup" exact component={Signup} props={childProps} />
    <AuthenticatedRoute path="/notes" exact component={Notes} props={childProps} />
    <AuthenticatedRoute path="/notes/new" exact component={NewNote} props={childProps} />
    <AuthenticatedRoute path="/notes/:id" exact component={EditNote} props={childProps} />
    <AuthenticatedRoute path="/finance" exact component={Balance} props={childProps} />
    <AuthenticatedRoute path="/finance/new" exact component={NewBalance} props={childProps} />
    <AuthenticatedRoute path="/finance/:id" exact component={EditBalance} props={childProps} />
    <AuthenticatedRoute path="/card" exact component={Card} props={childProps} />
    <AuthenticatedRoute path="/dash" exact component={DashStats} props={childProps} />
    
    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>;