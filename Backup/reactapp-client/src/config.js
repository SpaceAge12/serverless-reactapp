export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
      REGION: "eu-central-1",
      BUCKET: "serverless-reactapp-api-dev-attachmentsbucket-1abk331l59ny4"
    },
    apiGateway: {
      REGION: "eu-central-1",
      URL: "https://uv6ql7bobd.execute-api.eu-central-1.amazonaws.com/dev/"
    },
    cognito: {
      REGION: "eu-central-1",
      USER_POOL_ID: "eu-central-1_P3akI5qo2",
      APP_CLIENT_ID: "5pq3l8dbaa34lcfslj3bbv3cr0",
      IDENTITY_POOL_ID: "eu-central-1:58493248-786b-4787-afa5-dc8ae993cf8e"
    }
  };