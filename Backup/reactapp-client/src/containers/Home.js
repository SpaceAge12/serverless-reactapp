import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Home.css";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      notes: []
    };
  }

  renderLander() {
    return (
      <div className="lander">
        <h1>Coming Soon</h1>
        <p>Your all in one website solution</p>
        
        {this.props.isAuthenticated
                ? <div>
                  </div>
                : <div>
                    <Link to="/login" className="btn btn-info btn-lg">
                        Login
                    </Link>
                    <Link to="/signup" className="btn btn-success btn-lg">
                      Signup
                    </Link>
                  </div>
              }
      </div>
    );
  }


  render() {
    return (
      <div className="Home">
        {this.renderLander()}
      </div>
    );
  }
}