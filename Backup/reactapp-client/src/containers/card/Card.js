import React, { Component } from "react";
import { PageHeader, Table } from "react-bootstrap";
import { API } from "aws-amplify";
import { VictoryBar, VictoryChart, VictoryAxis, VictoryGroup, VictoryLine, VictoryLabel, VictoryPie } from 'victory';
import { formatDateDayMonth, formatDateMonthYear, dateDiffInDays, dateDiffInMonths, formatDateLocal } from '../../assets/DateFormatter';
import "./Card.css";



export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      transactions: null,
      dailyChartData: null,
      monthlyChartData: null
    };
  }

  async componentDidMount() {
    if (!this.props.isAuthenticated) {
      return;
    }

    try {
      const transactions = await this.getTransactions();
      this.setState({ transactions })
      this.getDailyData()
      this.getMonthlyData()
      this.getCategoryData()
    } catch (e) {
      alert(e);
    }
    this.setState({ isLoading: false });
  }

  getTransactions() {
    return API.get("API-Endpoint", "card");
  }

  renderTransactionTable() {

    return (
      <div className="StatsTable">
        <Table>
          <thead>
            <tr>
              <th><div className="padding">
                Date
              </div></th>
              <th><div className="padding">
                Name
              </div></th>
              <th><div className="padding">
                Category
              </div></th>
              <th><div className="padding">
                Code
              </div></th>
              <th><div className="padding">
                Amount
              </div></th>
            </tr>
          </thead>
          <tbody>
            {this.getTransactionInfo(this.state.transactions)}
          </tbody>
        </Table>
      </div>
    )
  }

  getTransactionInfo(transactions) {
    return [{}].concat(transactions).map(
      (item, i) =>
        item.amount < 0 ?
          <tr>
            <td>
              <div className="padding">
                {formatDateLocal(item.createdAt)}
              </div>
            </td>
            <td>
              <div className="padding">
                {item.merchant.name.toUpperCase()}
              </div>
            </td>
            <td>
              <div className="padding">
                {item.merchant.category}
              </div>
            </td>
            <td>
              <div className="padding">
                {item.merchant.category_code}
              </div>
            </td>
            <td>
              <div className="padding">
                R {parseInt(item.amount / 100).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}
              </div>
            </td>
          </tr>
          :
          ""
    );
  }

  getDailyData() {
    var dailyChartData = [];
    let today = new Date(Date.now());

    let daysDiff = dateDiffInDays(this.state.transactions[this.state.transactions.length - 1].createdAt, today);

    for (let i = 0; i <= daysDiff; i++) {
      let dateVariable = new Date();
      dateVariable.setDate((today.getDate() - i))
      dailyChartData.push({
        income: 0,
        expense: 0,
        rollingBalance: 0,
        date: formatDateDayMonth(dateVariable)
      })
    }

    //Merging transaction JSON into daily data Array @dailyChartData[]
    this.state.transactions.forEach(element => {
      let amount = parseFloat(element.amount) / 100;
      daysDiff = dateDiffInDays(element.createdAt, today);

      if (amount > 0) {
        dailyChartData[daysDiff].income = parseFloat(dailyChartData[daysDiff].income) + amount
      }
      else {
        dailyChartData[daysDiff].expense = parseFloat(dailyChartData[daysDiff].expense) + Math.abs(amount)
      }
    });

    dailyChartData[dailyChartData.length - 1].rollingBalance = dailyChartData[dailyChartData.length - 1].income + dailyChartData[dailyChartData.length - 1].expense

    for (var i = dailyChartData.length - 2; i >= 0; i--) {
      dailyChartData[i].rollingBalance = dailyChartData[i].income - dailyChartData[i].expense + dailyChartData[i + 1].rollingBalance
    }

    this.setState({ dailyChartData })

  }

  getMonthlyData() {
    var monthlyChartData = [];
    let today = new Date(Date.now());

    for (let i = 0; i <= 12; i++) {
      let dateVariable = new Date();
      dateVariable.setMonth((today.getMonth() - i))
      monthlyChartData.push({
        income: 0,
        expense: 0,
        date: formatDateMonthYear(dateVariable)
      })
    }

    //Merging transaction JSON into daily data Array @dailyChartData[]
    this.state.transactions.forEach(element => {
      let amount = parseFloat(element.amount) / 100;
      let monthsDiff = dateDiffInMonths(element.createdAt, today);
      if (amount > 0) {
        monthlyChartData[monthsDiff].income = parseFloat(monthlyChartData[monthsDiff].income) + amount
      }
      else {
        monthlyChartData[monthsDiff].expense = parseFloat(monthlyChartData[monthsDiff].expense) + Math.abs(amount)
      }
    });

    this.setState({ monthlyChartData })
  }

  getCategoryData() {
    var dailyChartData = [];
    let today = new Date(Date.now());

    //Merging transaction JSON into daily data Array @dailyChartData[]
    this.state.transactions.forEach(element => {
      let amount = parseFloat(element.amount) / 100;
      daysDiff = dateDiffInDays(element.createdAt, today);

      if (amount > 0) {
        dailyChartData[daysDiff].income = parseFloat(dailyChartData[daysDiff].income) + amount
      }
      else {
        dailyChartData[daysDiff].expense = parseFloat(dailyChartData[daysDiff].expense) + Math.abs(amount)
      }
    });

    dailyChartData[dailyChartData.length - 1].rollingBalance = dailyChartData[dailyChartData.length - 1].income + dailyChartData[dailyChartData.length - 1].expense

    for (var i = dailyChartData.length - 2; i >= 0; i--) {
      dailyChartData[i].rollingBalance = dailyChartData[i].income - dailyChartData[i].expense + dailyChartData[i + 1].rollingBalance
    }

    this.setState({ dailyChartData })

  }

  renderStatsTable() {
    let overview = [
      {
      name: "Available",
      value: this.state.dailyChartData[0].rollingBalance
      },
      {
        name: "Current Month",
        value: this.state.monthlyChartData[0].expense
        },
      {
        name: "Last Month",
        value: this.state.monthlyChartData[1].expense
        }
  ]

    return (
      <div className="StatsTable">
        <Table>
          <tbody>
            {this.renderStats(overview)}
          </tbody>
        </Table>
      </div>
    )
  }

  renderStats(list) {
    return [{}].concat(list).map(
      (item, i) =>
        i !== 0 ?
          <tr>
            <td>
              <div className="alignleft padding">
                {item.name}
              </div>
            </td>
            <td>
              <div className="alignright padding">
                R {parseInt(item.value).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}
              </div>
            </td>
          </tr>
          :
          ""
    );
  }

  renderDailyTransactionChart() {

    return (
      <div className="dailyTransactionChart">
        <VictoryChart
          domainPadding={10}>
          <VictoryLabel x={180} y={35}
            text={"Daily transactions"}
            style={{
              fill: "white"
            }}
          />
          <VictoryAxis dependentAxis
            style={{
              tickLabels: { angle: 0, fontSize: 5, fill: "white" },
              axis: { stroke: "white" },
              grid: { stroke: "#000" }
            }}
          />

          <VictoryAxis
            style={{
              tickLabels: { angle: 90, fontSize: 5, fill: "white" },
              axis: { stroke: "white" },
              grid: { stroke: "#000", strokeWidth: 1, strokeDasharray: 10 }
            }}
          />

          <VictoryBar
            alignment="end"
            style={{ data: { fill: "#c43a31" } }}
            data={this.state.dailyChartData.slice(0, 31).reverse()}
            // data accessor for x values
            x="date"
            // data accessor for y values
            y="expense"
          />

        </VictoryChart>
      </div>
    );
  }

  renderMonthlyTransactionChart() {

    return (
      <div className="monthlyTransactionChart">
        <VictoryChart
          domainPadding={10}>
          <VictoryLabel x={180} y={35}
            text={"Monthly transactions"}
            style={{
              fill: "white"
            }}
          />
          <VictoryAxis dependentAxis
            style={{
              tickLabels: { angle: 0, fontSize: 5, fill: "white" },
              axis: { stroke: "white" },
              grid: { stroke: "#000" }
            }}
          />

          <VictoryAxis
            style={{
              tickLabels: { angle: 90, fontSize: 5, fill: "white" },
              axis: { stroke: "white" },
              grid: { stroke: "#000", strokeWidth: 1, strokeDasharray: 10 }
            }}
          />

          <VictoryBar
            alignment="end"
            style={{ data: { fill: "#c43a31" } }}
            data={this.state.monthlyChartData.slice().reverse()}
            // data accessor for x values
            x="date"
            // data accessor for y values
            y="expense"
          />

        </VictoryChart>
      </div>
    );
  }

  renderRollingBalanceChart() {

    return (
      <div className="rollingBalanceChart">
        <VictoryChart
          domainPadding={10}>
          <VictoryLabel x={180} y={35}
            text={"Current balance"}
            style={{
              fill: "white"
            }}
          />
          <VictoryAxis dependentAxis
            axisLabelComponent={<VictoryLabel text="CLQ Aggregate" />}
            style={{
              tickLabels: { angle: 0, fontSize: 5, fill: "white" },
              axis: { stroke: "white" },
              grid: { stroke: "#000" }
            }}
          />

          <VictoryAxis
            style={{
              tickLabels: { angle: 90, fontSize: 5, fill: "white" },
              axis: { stroke: "white" },
              grid: { stroke: "#000", strokeWidth: 1, strokeDasharray: 10 }
            }}
          />

          <VictoryLine
            alignment="end"
            style={{
              data: { stroke: "#c43a31" },
              parent: { border: "1px solid #ccc" }
            }}
            interpolation="linear"
            data={this.state.dailyChartData.slice(0, 31).reverse()}
            // data accessor for x values
            x="date"
            // data accessor for y values
            y="rollingBalance"
          />

        </VictoryChart>
      </div>
    );
  }

  renderCategoryChart(){
    return (
      <div className="categoryChart">

          <VictoryPie
            height={200}
            width={200}
            style={{
              data: { stroke: "#c43a31" }
            }}
            data={this.state.dailyChartData.slice(0, 31).reverse()}
            // data accessor for x values
            x="date"
            // data accessor for y values
            y="rollingBalance"
          />
      </div>
    );
  
  }

  render() {
    return (
      <div className="Home">
        <PageHeader>Root Card</PageHeader>
        {!this.state.isLoading && this.renderStatsTable()}
        {!this.state.isLoading && this.renderRollingBalanceChart()}
        {!this.state.isLoading && this.renderDailyTransactionChart()}
        {!this.state.isLoading && this.renderMonthlyTransactionChart()}
        {!this.state.isLoading && this.renderCategoryChart()}
        {!this.state.isLoading && this.renderTransactionTable()}
        

      </div>
    );
  }
}