import React, { Component } from "react";
//import { Link } from "react-router-dom";
import "./Stats.css";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import { API } from "aws-amplify";

export default class Stats extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      data: []
    };
  }

  async componentDidMount() {
    if (!this.props.isAuthenticated) {
      return;
    }

    try {
      const data = await this.data();
      this.setState({ data });
    } catch (e) {
      alert(e);
    }

    this.setState({ isLoading: false });
  }

  data() {
    return API.get("API-Endpoint", "dash");
  }

  renderChart() {
      const data = [
          {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
          {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
          {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
          {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
          {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
          {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
          {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
        ];
    return (
        <div className="chart">
    		<BarChart width={900} height={250} data={data}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="pv" fill="#8884d8" />
              <Bar dataKey="uv" fill="#82ca9d" />
            </BarChart>
	    </div>
    );
  }


  render() {
    return (
      <div className="Home">
        {this.renderChart()}
      </div>
    );
  }
}