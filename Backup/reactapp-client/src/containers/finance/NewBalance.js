import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { API } from "aws-amplify";
import LoaderButton from "../../components/LoaderButton";
import "./NewBalance.css";

export default class NewNote extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: null,
            name: "",
            amount: "",
            type: "income"
        };
    }

    validateForm() {
    return this.state.name.length > 0 && this.state.amount.length > 0;
    }
    
    handleChange = event => {
      this.setState({
      [event.target.id]: event.target.value
    });
    }

    handleSubmit = async event => {
        event.preventDefault();

        this.setState({ isLoading: true });

        try {
            await this.createEntry({
                name: this.state.name,
                amount: this.state.amount,
                type: this.state.type
            });
            this.props.history.push("/");
        } catch (e) {
            alert(e);
            this.setState({ isLoading: false });
        }
    }

    createEntry(entry) {
        return API.post("API-Endpoint", "finance", {
            body: entry
        });
    }

    render() {
        return (
        <div className="NewBalance">
            <form onSubmit={this.handleSubmit}>
              <FormGroup controlId="name" bsSize="large">
                <ControlLabel>Name</ControlLabel>
                <FormControl
                  autoFocus
                  type="text"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup controlId="amount" bsSize="large">
                <ControlLabel>Amount</ControlLabel>
                <FormControl
                  value={this.state.amount}
                  onChange={this.handleChange}
                  type="text"
                />
              </FormGroup>
              <FormGroup controlId="type" bsSize="large">
               <ControlLabel>Type</ControlLabel>
                  <FormControl value={this.state.type}
                          onChange={this.handleChange}
                          componentClass="select">
                    <option value="income">Income</option>
                    <option value="expense">Expense</option>
                    <option value="asset">Asset</option>
                    <option value="liability">Liability</option>
                  </FormControl>
              </FormGroup>
              <LoaderButton
                block
                bsSize="large"
                disabled={!this.validateForm()}
                type="submit"
                isLoading={this.state.isLoading}
                text="Add"
                loadingText="Adding..."
              >
              </LoaderButton>
            </form>
        </div>

        );
    }
}

