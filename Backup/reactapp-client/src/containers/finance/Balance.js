import React, { Component } from "react";
import { PageHeader, ListGroup, ListGroupItem, Glyphicon, Table } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { VictoryPie } from 'victory';
import { API } from "aws-amplify";
import "./Balance.css";



export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      income: [],
      expenses: [],
      assets: [],
      liabilities: [],
      total: [],
      entries: []
    };
  }

  async componentDidMount() {
    if (!this.props.isAuthenticated) {
      return;
    }

    try {
      const entries = await this.balanceEntries();
      let income = [];
      let expenses = [];
      let assets = [];
      let liabilities = [];
      let total = {
        income: 0,
        expenses: 0,
        assets: 0,
        liabilities: 0
      };
      entries.forEach(function (element) {
        if (element.type === "income") {
          income.push(element);
          total.income = parseInt(total.income) + parseInt(element.amount);
        }
        else if (element.type === "expense") {
          expenses.push(element);
          total.expenses = parseInt(total.expenses) + parseInt(element.amount);
        }
        else if (element.type === "asset") {
          assets.push(element);
          total.assets = parseInt(total.assets) + parseInt(element.amount);
        }
        else {
          liabilities.push(element);
          total.liabilities = parseInt(total.liabilities) + parseInt(element.amount);
        }

      });

      console.log(entries);
      console.log(income);
      this.setState({ income, expenses, assets, liabilities, total });
    } catch (e) {
      alert(e);
    }

    this.setState({ isLoading: false });
  }

  balanceEntries() {
    return API.get("API-Endpoint", "finance");
  }

  renderEntriesList(entries) {
    return [{}].concat(entries).map(
      (entry, i) =>
        i !== 0
          ? <LinkContainer
            key={entry.financeId}
            to={`/finance/${entry.financeId}`}
          >
            <ListGroupItem>
              <Table bordered={false} className="rowEntryTable">
                <tr>
                  <th>
                    <div className="alignleft">
                      {entry.name.trim().split("\n")[0]}
                    </div>
                  </th>
                  <th>
                    <div className="alignright">
                      {parseInt(entry.amount).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}
                    </div>
                  </th>
                </tr>
              </Table>
            </ListGroupItem>
          </LinkContainer>
          : <LinkContainer
            key="new"
            to="/finance/new"
          >
            <ListGroupItem>
              <div className="center">
                <Glyphicon glyph="plus" className="spinning" />
              </div>
            </ListGroupItem>
          </LinkContainer>
    );
  }

  renderGridHeader(header) {
    return <h4>
      <b>{header}</b>
    </h4>
  };

  renderTotal(amount) {
    return <ListGroup>
      <ListGroupItem>
        <Table bordered={false} className="totalTable">
          <tr>
            <th>
              <div className="alignleft">
                Total:
              </div>
            </th>
            <th>
              <div className="alignright">
                R {parseInt(amount).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}
              </div>
            </th>
          </tr>
        </Table>
      </ListGroupItem>
    </ListGroup>
  }

  renderSummary() {
    const nett = parseInt(this.state.total.income) - parseInt(this.state.total.expenses) + parseInt(this.state.total.assets) - parseInt(this.state.total.liabilities)

    let overview = [{
      name: "USEABLE",
      value: this.state.total.income - this.state.total.expenses
    },
    {
      name: "NETT",
      value: nett
    },
    {
      name: "PERCENTAGE",
      value: (nett / this.state.total.income) * 100
    }
    ]

    return (
      <div className="summary">
        <Table>
          <tr>
            <th>
              <Table className="summaryTable">
                {
                  this.renderOverviewList(overview)
                }
              </Table>
            </th>
            <th>
              {!this.state.isLoading && this.renderChart()}
            </th>
          </tr>
        </Table>
      </div>
    );
  }

  renderOverviewList(list) {
    return [{}].concat(list).map(
      (item, i) =>
        i !== 0 ?
        <tr>
          <th>
            <div className="alignleft padding">
            
              {item.name}
            </div>
          </th>
          <th>
            <div className="alignright padding">
              R {parseInt(item.value).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}
            </div>
          </th>
        </tr>
        :
        ""
    );
  }

  renderEntries() {
    return (
      <div className="notes">

        <Table bordered className="balanceSheetTable">
          <tr>
            <td>
              <div className="income padding ">
                {!this.state.isLoading && this.renderGridHeader("Income")}
              </div>
            </td>
            <td>
              <div className="expense padding ">
                {!this.state.isLoading && this.renderGridHeader("Expense")}
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <ListGroup>
                <span className="list-notes">
                  {!this.state.isLoading && this.renderEntriesList(this.state.income)}
                </span>
              </ListGroup>
            </td>
            <td>
              <ListGroup>
                <span className="list-notes">
                  {!this.state.isLoading && this.renderEntriesList(this.state.expenses)}
                </span>
              </ListGroup>
            </td>
          </tr>
          <tr>
            <td>
              <div className="total">
                {!this.state.isLoading && this.renderTotal(this.state.total.income)}
              </div>
            </td>
            <td>
              <div className="total">
                {!this.state.isLoading && this.renderTotal(this.state.total.expenses)}
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div className="income padding ">
                {!this.state.isLoading && this.renderGridHeader("Asset")}
              </div>
            </td>
            <td>
              <div className="expense padding ">
                {!this.state.isLoading && this.renderGridHeader("Liability")}
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <ListGroup>
                <span className="list-notes">
                  {!this.state.isLoading && this.renderEntriesList(this.state.assets)}
                </span>
              </ListGroup>
            </td>
            <td>
              <ListGroup>
                <span className="list-notes">
                  {!this.state.isLoading && this.renderEntriesList(this.state.liabilities)}
                </span>
              </ListGroup>
            </td>
          </tr>
          <tr>
            <td>
              <div className="total">
                {!this.state.isLoading && this.renderTotal(this.state.total.assets)}
              </div>
            </td>
            <td>
              <div className="total">
                {!this.state.isLoading && this.renderTotal(this.state.total.liabilities)}
              </div>
            </td>
          </tr>
        </Table>
      </div>
    );
  }

  renderChart() {
    let expensesNett = [];
    let expensesIncome = [];
    expensesNett.push({
      month: 0,
      expense: parseInt(this.state.total.income) - parseInt(this.state.total.expenses),
      fill: "#17361e"
    });

    for (let i = 1; i < this.state.expenses.length; i++) {
      expensesNett.push({
        month: i,
        expense: parseInt(this.state.expenses[i].amount),
        fill: "#400b10",
      });
    }

    expensesIncome.push({
      month: 0,
      expense: parseInt(this.state.total.income),
      fill: "#17361e"
    });
    expensesIncome.push({
      month: 1,
      expense: parseInt(this.state.total.expenses),
      fill: "#400b10"
    });


    return (
      <div className="chart">
        <VictoryPie width={400} height={200}
          data={expensesNett}
          // data accessor for x values
          x="expense"
          // data accessor for y values
          y="month"
          innerRadius={70} labelRadius={90}  
          style={{ labels: { fontSize: 8, fill: "white"}}}
        />
      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        <PageHeader>Your Balance</PageHeader>
        {!this.state.isLoading && this.renderSummary()}
        <hr></hr>
        {!this.state.isLoading && this.renderEntries()}
      </div>
    );
  }
}