import React, { Component } from "react";
import { API } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../../components/LoaderButton";
import "./EditBalance.css";

export default class Notes extends Component {
    constructor(props) {
        super(props);


        this.state = {
            isLoading: null,
            isDeleting: null,
            balance: null,
            name: "",
            amount: "",
            type: null
        };
    }

    async componentDidMount() {
        try {
            const balance = await this.getBalance();
            const { name, amount, type } = balance;

            this.setState({
                name,
                amount,
                type
            });
        } catch (e) {
            alert(e);
        }
    }

    getBalance() {
        return API.get("API-Endpoint", `finance/${this.props.match.params.id}`);
    }


    validateForm() {
        return this.state.name.length > 0 && this.state.amount.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    saveBalance(balance) {
        return API.put("API-Endpoint", `finance/${this.props.match.params.id}`, {
            body: balance
        });
    }

    handleSubmit = async event => {
        event.preventDefault();


        this.setState({ isLoading: true });

        try {
            await this.saveBalance({
                name: this.state.name,
                amount: this.state.amount,
                type: this.state.type
            });
            this.props.history.push("/");
        } catch (e) {
            alert(e);
            this.setState({ isLoading: false });
        }
    }

    deleteBalance() {
        return API.del("API-Endpoint", `finance/${this.props.match.params.id}`);
    }

    handleDelete = async event => {
        event.preventDefault();

        const confirmed = window.confirm(
            "Are you sure you want to delete this note?"
        );

        if (!confirmed) {
            return;
        }

        this.setState({ isDeleting: true });

        try {
            await this.deleteBalance();
            this.props.history.push("/");
        } catch (e) {
            alert(e);
            this.setState({ isDeleting: false });
        }
    }

    render() {
        return (
            <div className="NewBalance">
            <form onSubmit={this.handleSubmit}>
              <FormGroup controlId="name" bsSize="large">
                <ControlLabel>Name</ControlLabel>
                <FormControl
                  autoFocus
                  type="text"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup controlId="amount" bsSize="large">
                <ControlLabel>Amount</ControlLabel>
                <FormControl
                  value={this.state.amount}
                  onChange={this.handleChange}
                  type="text"
                />
              </FormGroup>
              <FormGroup controlId="type" bsSize="large">
               <ControlLabel>Type</ControlLabel>
                  <FormControl value={this.state.type}
                          onChange={this.handleChange}
                          componentClass="select">
                    <option value="income">Income</option>
                    <option value="expense">Expense</option>
                    <option value="asset">Asset</option>
                    <option value="liability">Liability</option>
                  </FormControl>
              </FormGroup>
                <LoaderButton
                    block
                    bsStyle="primary"
                    bsSize="large"
                    disabled={!this.validateForm()}
                    type="submit"
                    isLoading={this.state.isLoading}
                    text="Save"
                    loadingText="Saving…"
                />
                <LoaderButton
                    block
                    bsStyle="danger"
                    bsSize="large"
                    isLoading={this.state.isDeleting}
                    onClick={this.handleDelete}
                    text="Delete"
                    loadingText="Deleting…"
                />
            </form>
        </div>

        );
    }
}