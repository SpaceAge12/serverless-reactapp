import { LinkContainer } from "react-router-bootstrap";
import React, { Component, Fragment } from 'react';
import { Link, withRouter } from "react-router-dom";
import { Navbar, Nav, NavItem } from "react-bootstrap";
import { Auth } from "aws-amplify";
import "./App.css";
import Routes from "./Routes";

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isAuthenticating: true
    };
  }

  async componentDidMount() {
    try {
      await Auth.currentSession();
      this.userHasAuthenticated(true);
    }
    catch (e) {
      if (e !== 'No current user') {
        alert(e);
      }
    }

    this.setState({ isAuthenticating: false });
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated })
  }

  handleLogout = async event => {
    await Auth.signOut();
    this.userHasAuthenticated(false);
    this.props.history.push("/login");
  }

  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated
    };

    return (!this.state.isAuthenticating &&
      <div className="App container">
        <Navbar fluid inverse collapseOnSelect>
            <Navbar.Brand>
              <Link to="/">SpaceAge-12</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
         
          <Navbar.Collapse>
          
          <Nav>
            <LinkContainer to="/notes">
              <NavItem>
                <span className="navItem">Notes</span>
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/card">
                <NavItem>
                  <span className="navItem">Card</span>
                </NavItem>
            </LinkContainer>
            <LinkContainer to="/finance">
                <NavItem>
                  <span className="navItem">Finance</span>
                </NavItem>
            </LinkContainer>
          </Nav>
          
            <Nav pullRight>
              {this.state.isAuthenticated
                ? <NavItem onClick={this.handleLogout}><span className="navItem">Logout</span></NavItem>
                : <Fragment>
                  <LinkContainer to="/signup">
                    <NavItem><span className="navItem">Signup</span></NavItem>
                  </LinkContainer>
                  <LinkContainer to="/login">
                    <NavItem><span className="navItem">Login</span></NavItem>
                  </LinkContainer>
                </Fragment>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Routes childProps={childProps} />
      </div>
    );
  }
}

export default withRouter(App);
