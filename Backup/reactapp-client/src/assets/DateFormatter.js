const _MS_PER_DAY = 1000 * 60 * 60 * 24;

export const formatDateDayMonth = (rawDate) =>{
    let dateVar = new Date(rawDate);
    let formattedDate = dateVar.getDate() + "/" + ("0"+ (dateVar.getMonth() + 1)).split(-2)  ;//+"/" +dateVar.getFullYear();
    return formattedDate;
}

export const formatDateMonthYear = (rawDate) =>{
    let dateVar = new Date(rawDate);
    let formattedDate = ("0"+ (dateVar.getMonth() + 1)).split(-2)  +"/" +dateVar.getFullYear();
    return formattedDate;
}

export const dateDiffInDays = (a, b) => {
 // Discard the time and time-zone information.
    let dateVarA = new Date(a);
    let dateVarB = new Date(b);
    const utc1 = Date.UTC(dateVarA.getFullYear(), dateVarA.getMonth(), dateVarA.getDate());
    const utc2 = Date.UTC(dateVarB.getFullYear(), dateVarB.getMonth(), dateVarB.getDate());
 return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

export const dateDiffInMonths = (previous, current) => {
    // Discard the time and time-zone information.
       let previousDate = new Date(previous);
       let currentDate = new Date(current);

       if (currentDate.getFullYear() > previousDate.getFullYear()){
        return Math.abs(11-(previousDate.getMonth() - currentDate.getMonth()));
       }
       else{
        return Math.abs(currentDate.getMonth() - previousDate.getMonth());
    }
   }

export const formatDateLocal = (rawDate) => {
    let dateVar = new Date(rawDate);
    let formattedDate =  ("0"+ (dateVar.getDate())).slice(-2) + "/" + ("0"+ (dateVar.getMonth() + 1)).slice(-2) +"/" +dateVar.getFullYear() + " " + dateVar.getHours() + ":" + ("0"+ (dateVar.getMinutes())).slice(-2) ;
    return formattedDate;
}