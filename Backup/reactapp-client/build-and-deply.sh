#!/bin/bash
npm run build
aws s3 sync . s3://serverless-reactapp-frontend --region eu-central-1 --delete
aws cloudfront create-invalidation --distribution-id E1DOYDCWCFFI0B --paths "/*"