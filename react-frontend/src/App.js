import { STATE_LOGIN, STATE_SIGNUP } from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, UnauthenticatedRoute, AuthenticatedRoute, MainLayout } from 'components/Layout';
import AuthPage from 'pages/AuthPage';
import HomePage from 'pages/HomePage';
import CvPage from 'pages/CvPage';
import ShoppingPage from 'pages/ShoppingPage';
import CasesPage from 'pages/CasesPage'
// pages
import DashboardPage from 'pages/DashboardPage';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Switch } from 'react-router-dom';
import { Auth } from "aws-amplify";
import './styles/reduction.scss';

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isAuthenticating: true
    };
  }

  async componentDidMount() {
    try {
      await Auth.currentSession();
      this.userHasAuthenticated(true);
    }
    catch (e) {
      if (e !== 'No current user') {
        alert(e);
      }
    }

    this.setState({ isAuthenticating: false });
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated })
  }

  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated,
      authState: STATE_LOGIN
    };

    return (
      <BrowserRouter basename={getBasename()}>
        <GAListener>
          <Switch>
            <UnauthenticatedRoute
              exact
              path="/login"
              layout={EmptyLayout}
              component={AuthPage}
              props={childProps}
            />
            <LayoutRoute
              exact
              path="/signup"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_SIGNUP} />
              )}
            />
            <LayoutRoute
              exact
              path="/cv"
              layout={MainLayout}
              component={CvPage}
              props={childProps}
            />
            <LayoutRoute
              exact
              path="/"
              layout={MainLayout}
              component={HomePage}
              props={childProps}
            />
            <AuthenticatedRoute
              exact
              path="/dashboard"
              layout={MainLayout}
              component={DashboardPage}
              props={childProps}
            />
            <AuthenticatedRoute
              exact
              path="/cases"
              layout={MainLayout}
              component={CasesPage}
              props={childProps}
            />
            <AuthenticatedRoute
              exact
              path="/shopping"
              layout={MainLayout}
              component={ShoppingPage}
              props={childProps}
            />
            <LayoutRoute
              exact
              path="/register"
              layout={MainLayout}
              component={AuthPage}
            />
            <Redirect to="/" />
          </Switch>
        </GAListener>
      </BrowserRouter>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
