import Page from 'components/Page';
import { NumberWidget } from 'components/Widget';
import { stackLineChartOptions } from 'demos/chartjs';
import { getDailyData, getMonthlyData, getTransactionTable } from 'scripts/formatTransactions';
import { getExpensesGraph, getPieOverviewGraph, getYearlyOverviewGraph, getRollingBalanceGraph } from 'scripts/dashboardGraphs';
import {
  chartjs,
} from 'demos/dashboardPage';
import React from 'react';
import { Bar, Line, Doughnut } from 'react-chartjs-2';
import {
  MdInsertChart,
} from 'react-icons/md';
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Table
} from 'reactstrap';
import { API } from 'aws-amplify';

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      transactions: null,
      dailyChartData: null,
      monthlyChartData: null
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    if (!this.props.isAuthenticated) {
      return;
    }

    try {
      const transactions = await this.getTransactions();
      this.setState({ transactions })
      this.setState({ dailyChartData: getDailyData(this.state.transactions) });
      this.setState({ monthlyChartData: getMonthlyData(this.state.transactions) });


    } catch (e) {
      alert(e);
    }
    this.setState({ isLoading: false });
  }

  getTransactions() {
    return API.get("API-Endpoint", "card");
  }

  render() {
    return (
      <Page
        className="DashboardPage"
        title="Dashboard"
        breadcrumbs={[{ name: 'Dashboard', active: true }]}
      >
        <Row>
          <Col lg="6" md="12" sm="12" xs="12">
            <NumberWidget
              title="Current Balance"
              subtitle=''
              number="5k"
              color="secondary"
              progress={{
                value: (!this.state.isLoading) ? parseInt(this.state.dailyChartData.rollingBalance[0] / 50) : 100,
                label: (!this.state.isLoading) ? "R " + parseInt(this.state.dailyChartData.rollingBalance[0]) : 100,
              }}
            />

          </Col>
          <Col lg="6" md="12" sm="12" xs="12">
            <NumberWidget
              title="This Month"
              subtitle=""
              number="5k"
              color="secondary"
              progress={{
                value: (!this.state.isLoading) ? parseInt(100 - this.state.monthlyChartData.expense[0] / 50) : 100,
                label: (!this.state.isLoading) ? "R " + parseInt(this.state.monthlyChartData.expense[0]) : 100,
              }}
            />
          </Col>
        </Row>

        <Row>
          <Col lg="8" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                Expenses{' '}
                <small className="text-muted text-capitalize">Past 30 days</small>
              </CardHeader>
              <CardBody>
                <Bar data={getExpensesGraph(this.state.dailyChartData)} options={chartjs.bar.options} />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>Balance</CardHeader>
              <CardBody>
                <Bar data={getYearlyOverviewGraph(this.state.monthlyChartData)} options={chartjs.bar.options} />
                <Doughnut data={getPieOverviewGraph(this.state.monthlyChartData)} />
              </CardBody>

            </Card>

          </Col>
        </Row>

        <Row>
          <Col lg="8" md="12" sm="12" xs="12">
            <Card>
              <Line
                data={getRollingBalanceGraph(this.state.dailyChartData)} options={stackLineChartOptions}
              />
              <CardBody
                className="text-primary"
                style={{ position: 'absolute' }}
              >
                <CardTitle>
                  <MdInsertChart /> Rolling Balance
                </CardTitle>
              </CardBody>
            </Card>
          </Col>

          <Col>
            <Card lg="3" md="12" sm="12" xs="12">
              <Table {...{ ['hover']: true }}>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  {!this.state.isLoading && getTransactionTable(this.state.transactions)}
                </tbody>
              </Table>
            </Card>
          </Col>

        </Row>
      </Page>
    );
  }
}
export default DashboardPage;
