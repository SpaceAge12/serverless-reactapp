import Page from 'components/Page';
import { UserCard, } from 'components/Card'
import me from 'assets/img/users/me.jpg'
import { getSkills, getExperience } from 'scripts/cvPageData'
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import WorkIcon from '@material-ui/icons/Work';
import SchoolIcon from '@material-ui/icons/School';
import 'react-vertical-timeline-component/style.min.css';
import React from 'react';
import {
    MdInsertChart,
} from 'react-icons/md';
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Progress
} from 'reactstrap';

class DashboardPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
        };
    }

    async componentDidMount() {
        window.scrollTo(0, 0);
        if (!this.props.isAuthenticated) {
            return;
        }

        try {

        } catch (e) {
            alert(e);
        }
        this.setState({ isLoading: false });
    }


    render() {
        return (
            <Page
                className="CvPage"
                title="CV"
                breadcrumbs={[{ name: 'cv', active: true }]}
            >
                <Row>
                    <Col lg="12" md="12" sm="12" xs="12">
                        <UserCard
                            avatar={me}
                            title="Liam Coetzee"
                            subtitle="Electrical and Electronic Engineer/Software Developer"
                            text="email: coetzeeliam@gmail.com"
                            style={{
                                height: 300,
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg="6" md="12" sm="12" xs="12">
                        <Card>
                            <CardHeader>About Me</CardHeader>
                            <CardBody>
                                Making a cv is harder than I thought.
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="6" md="12" sm="12" xs="12">
                        <Card>
                            <CardHeader>Skillset</CardHeader>
                            <CardBody>
                                {getSkills().map((skills, index) => {
                                    return (
                                        <Row>
                                            <Col lg="3" md="3" sm="3" xs="3">
                                                {skills.name}
                                            </Col>
                                            <Col lg="9" md="9" sm="9" xs="9">
                                                <Progress
                                                    key={index}
                                                    color={skills.color}
                                                    value={skills.rating}
                                                    className="mb-3"
                                                >
                                                    {skills.rating}%
                                        </Progress>
                                            </Col>
                                        </Row>
                                    );
                                })}
                            </CardBody>
                        </Card>


                    </Col>
                </Row>

                <Row>
                    <Col lg="12" md="12" sm="12" xs="12">
                        <Card>
                            <VerticalTimeline>
                                {getExperience().map((experience, index) => {
                                    return (
                                        <VerticalTimelineElement
                                            className="vertical-timeline-element--work"
                                            date={experience.date}
                                            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
                                            icon={(experience.icon === 'work')? <WorkIcon /> : <SchoolIcon />}
                                        >
                                            <h3 className="vertical-timeline-element-title">{experience.title}</h3>
                                            <h4 className="vertical-timeline-element-subtitle">{experience.subtitle}</h4>
                                            <p>
                                                {experience.description}
                                            </p>
                                        </VerticalTimelineElement>);
                                })
                                }
                                <VerticalTimelineElement
                                    iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
                                    icon={MdInsertChart}
                                />
                            </VerticalTimeline>
                        </Card>
                    </Col>
                </Row>
            </Page>
        );
    }
}
export default DashboardPage;
