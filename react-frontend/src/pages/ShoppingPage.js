import IconButton from 'components/Widget/IconButton';
import Page from 'components/Page';
import React, { Fragment } from 'react';
import { API } from "aws-amplify";
import {
  Button,
  Card,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  Form,
  Input,
} from 'reactstrap';

class ShoppingPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      items: [],
      item: ''
    };

  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    if (!this.props.isAuthenticated) {
      return;
    }
    const shoppingList = await this.shoppinglist();
    this.state.items = shoppingList
    try {

    } catch (e) {
      alert(e);
    }
    this.setState({ isLoading: false });
  }

  addItem = async event => {
    event.preventDefault();
    await this.addListItem({
      item: this.state.item,
      state: "Get"
    })
    this.state.item="";
    const shoppingList = await this.shoppinglist();
    this.state.items = shoppingList
    this.props.history.push("/shopping");
  }

  handleDelete = async itemId => {
    const confirmed = window.confirm(
      "Are you sure you want to delete this note?"
    );

    if (!confirmed) {
      return;
    }

    this.setState({ isDeleting: true });

    try {
      await this.deleteListItem(itemId);
      const shoppingList = await this.shoppinglist();
      this.state.items = shoppingList
      this.props.history.push("/shopping");
    } catch (e) {
      alert(e);
      this.setState({ isDeleting: false });
    }
  }

  transferItem = async (itemId, state) => {
    await this.updateListItem(itemId, state)
    const shoppingList = await this.shoppinglist();
    this.state.items = shoppingList
    this.props.history.push("/shopping");
  }

  shoppinglist() {
    return API.get("API-Endpoint", "shopping");
  }

  addListItem(item) {
    return API.post("API-Endpoint", "shopping", {
      body: item
    });
  }

  updateListItem(itemId, state) {
    let body = {state: state}
    return API.put("API-Endpoint", `shopping/${itemId}`, {
      body: body
    });
  }

  deleteListItem(itemId) {
    return API.del("API-Endpoint", `shopping/${itemId}`);
  }

  renderItems(items, state) {
    return [{}].concat(items).slice(1).map(
      (item, i) =>
        (item.state !== state)?
        <Fragment>
          <Row>
            <Col lg="11" md="10" sm="10" xs="10" className="no-padding">
              <ListGroupItem action className="grab" onClick={() => this.transferItem(item.itemId, state)}>
                {item.item}
              </ListGroupItem>
            </Col>
            <Col lg="1" md="1" sm="1" xs="1">
              <IconButton
                value={item.itemId}
                onIconClick={this.handleDelete}
              />
            </Col>
          </Row>
        </Fragment>
        :
        <Fragment></Fragment>
    );
  }

  render() {
    return (
      <Page title="Shopping">
        <Row>
          <Col>
            <Card>
              <Form onSubmit={this.addItem}>
                <Row>
                  <Col lg="11" md="10" sm="9" xs="8">
                    <Input className="mb-2" name="item" value={this.state.item} onChange={this.handleChange} />
                  </Col>
                  <Col lg="1" md="1" sm="1" xs="1">
                    <Button outline color="success" >
                      Add Item
                    </Button>
                  </Col>
                </Row>
              </Form>
              <Row>
                <Col lg="6" md="12" sm="12" xs="12">
                  <Card>
                    <CardHeader>Get it</CardHeader>
                    <ListGroup flush>
                      {this.renderItems(this.state.items, "Got")}
                    </ListGroup>
                  </Card>
                </Col>
                <Col lg="6" md="12" sm="12" xs="12">
                  <Card>
                    <CardHeader>Got it</CardHeader>
                    <ListGroup flush>
                      {this.renderItems(this.state.items, "Get")}
                    </ListGroup>
                  </Card>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Page>
    );
  };
}
export default ShoppingPage;
