import Page from 'components/Page';
import IconButton from 'components/Widget/IconButton';
import React, { Fragment } from 'react';
import { API } from "aws-amplify";
import {
    Button,
    Col,
    Row,
    Form,
    Input,
    Card,
    ListGroupItem,
    ButtonGroup,
    CardHeader,
    CardBody,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';
import {
    MdInsertChart,
} from 'react-icons/md';

class CasesPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            cases: [],
            caseId: '',
            caseIdInput: '',
            date: Date.now(),
            caseState: 'PMA',
            correspondances: '',
            correspondanceModal: false
        };

    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    async componentDidMount() {
        window.scrollTo(0, 0);
        if (!this.props.isAuthenticated) {
            return;
        }
        const cases = await this.getCases();
        this.state.cases = cases;
        console.log(this.state.cases)
        try {

        } catch (e) {
            alert(e);
        }
        this.setState({ isLoading: false });
    }

    addCaseSubmit = async event => {
        event.preventDefault();
        await this.addCase({
            caseIdInput: this.state.caseIdInput,
            date: this.state.date,
            correspondances: 1,
            caseState: this.state.caseState
        })
        this.setState({ isLoading: true });
        this.state.cases.push({
            caseId: this.state.caseIdInput,
            date: this.state.date,
            correspondances: 1,
            caseState: this.state.caseState
        })
        this.setState({ isLoading: false });
        this.state.caseIdInput = "";

        console.log(this.state.cases)

    }

    changeCaseState = async (caseId, caseState) => {
        console.log(caseId, caseState)
        this.setState({ isLoading: true });
        for (var i = 0; 0 < this.state.cases.length; i++) {
            if (this.state.cases[i].caseId === caseId) {

                this.state.cases[i].caseState = caseState
                break;
            }
        }
        this.setState({ isLoading: false });
        await this.updateCaseState(caseId, caseState)

    }


    handleDelete = async caseId => {
        const confirmed = window.confirm(
            "Are you sure you want to delete this case?"
        );

        if (!confirmed) {
            return;
        }

        this.setState({ isDeleting: true });

        try {
            for (var i = 0; 0 < this.state.cases.length; i++) {
                if (this.state.cases[i].caseId === caseId) {

                    this.state.cases.splice(i, 1);
                    break;
                }
            }
            await this.deleteCase(caseId);

        } catch (e) {
            alert(e);
            this.setState({ isDeleting: false });
        }
    }

    toggleCorrespondanceModal = (caseData) => {
        let caseId = caseData[0]
        let correspondances = caseData[1]
        this.setState({
            correspondanceModal: !this.state.correspondanceModal,
            caseId: caseId,
            correspondances: correspondances
        })
    }

    addCorrespondance = () => {
        console.log(this.state.caseId, this.state.correspondances);
        this.setState({
            correspondanceModal: !this.state.correspondanceModal
        })
    }

    getCases() {
        return API.get("API-Endpoint", "cases");
    }

    addCase(caseItem) {
        return API.post("API-Endpoint", "cases", {
            body: caseItem
        });
    }

    updateCaseState(caseId, caseState) {
        let body = { caseState: caseState }
        return API.put("API-Endpoint", `cases/${caseId}`, {
            body: body
        });
    }


    deleteCase(caseId) {
        return API.del("API-Endpoint", `cases/${caseId}`);
    }

    renderCases(cases) {
        return [{}].concat(cases).slice(1).map(
            (item, i) =>
                <Row key={item.caseId}>
                    <Col className="no-padding">
                        <ListGroupItem action className="grab" onClick={() => this.transferItem(item.caseId)}>
                            {item.caseId}
                        </ListGroupItem>
                    </Col>
                    <Col className="no-padding">
                        <ButtonGroup size="sm" className='vertical-center'>
                            {this.renderStateButtons(item.caseId, item.caseState)}
                        </ButtonGroup>
                    </Col >
                    <Col className="no-padding">
                    <Fragment>
                        {item.correspondances}
                    </Fragment>
                    
                    <IconButton
                            value={[item.caseId, item.correspondances]}
                            button={"add"}
                            onIconClick={this.toggleCorrespondanceModal}
                        />
                    </Col>
                    <Col className="no-padding">
                        <IconButton
                            value={item.caseId}
                            button={"delete"}
                            onIconClick={this.handleDelete}
                        />
                    </Col>
                </Row>
        );
    }

    renderStateButtons(caseId, caseState) {

        if (caseState === "PMA") {
            return <Fragment>
                <Button color="success" onClick={() => this.changeCaseState(caseId, "PMA")}>PMA</Button>
                <Button outline color="warning" onClick={() => this.changeCaseState(caseId, "PAA")}>PAA</Button>
                <Button outline color="info" onClick={() => this.changeCaseState(caseId, "RSD")}>RSD</Button>
            </Fragment>
        }
        else if (caseState === "PAA") {
            return <Fragment>
                <Button outline color="success" onClick={() => this.changeCaseState(caseId, "PMA")}>PMA</Button>
                <Button color="warning" onClick={() => this.changeCaseState(caseId, "PAA")}>PAA</Button>
                <Button outline color="info" onClick={() => this.changeCaseState(caseId, "RSD")}>RSD</Button>
            </Fragment>
        }
        else if (caseState === "RSD") {
            return <Fragment>
                <Button outline color="success" onClick={() => this.changeCaseState(caseId, "PMA")}>PMA</Button>
                <Button outline color="warning" onClick={() => this.changeCaseState(caseId, "PAA")}>PAA</Button>
                <Button color="info" onClick={() => this.changeCaseState(caseId, "RSD")}>RSD</Button>
            </Fragment>
        }
    }

    renderModal() {
        return <Card>
            <CardBody>
                <Modal isOpen={this.state.correspondanceModal}>
                    <ModalHeader >Modal title</ModalHeader>
                    <ModalBody>
                        dd
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => this.addCorrespondance()}>
                            Do Something
                    </Button>
                    </ModalFooter>
                </Modal>
            </CardBody>
        </Card>
    }



    render() {
        console.log(this.state.date);

        return (
            <Page title="Cases">
                <Form onSubmit={this.addCaseSubmit}>
                    <Row>
                        <Col lg="2" md="2" sm="2" xs="2">
                            <Input className="mb-2" name="caseId" value={this.state.caseIdInput} onChange={this.handleChange} />
                        </Col>
                        <Col lg="2" md="2" sm="2" xs="2">
                            <Input
                                type="date"
                                name="date"
                                value={this.state.date}
                                onChange={this.handleChange}
                                placeholder="date placeholder"
                            />
                        </Col>
                        <Col lg="1" md="1" sm="1" xs="1">
                            <Button outline color="success" >
                                Submit
                          </Button>
                        </Col>
                    </Row>
                </Form>

                <Card>
                    {!this.state.isLoading && this.renderCases(this.state.cases)}
                </Card>
                {this.renderModal()}
            </Page>
        );
    };
}

export default CasesPage;