import React from 'react';
import PropTypes from 'utils/propTypes';


import { ListGroupItem } from 'reactstrap';

class ListItemButton extends React.Component {
    handleClick = () => {
        this.props.onItemClick(this.props.value);
    }

    render() {
        return (
                <ListGroupItem
                    onClick={this.handleClick}
                >
                
                </ListGroupItem>
        );
    }
}


export default ListItemButton;
