import React from 'react';
import { MdDeleteForever, MdAddBox } from 'react-icons/md';

class IconButton extends React.Component {
    handleClick = () => {
        this.props.onIconClick(this.props.value);
    }

    renderDelete() {
        return (
            <MdDeleteForever
                size={30}
                className="text-secondary can-click"
                onClick={this.handleClick}
            />
        );
    }

    renderAdd() {
        return (
            <MdAddBox
                size={30}
                className="text-success can-click"
                onClick={this.handleClick}
            />
        );
    }

    render() {

        var button = "";
        if (this.props.button === "delete") {
            button = this.renderDelete()
        } else {
            button = this.renderAdd()
        }

        return (
            <div className='vertical-center'>
                {button}
            </div>
        );
    }
}


export default IconButton;
