import Avatar from 'components/Avatar';
import { UserCard } from 'components/Card';
import SearchInput from 'components/SearchInput';
import me from 'assets/img/users/me.jpg'
import React from 'react';
import {
  MdClearAll,
  MdExitToApp,
} from 'react-icons/md';
import {
  Button,
  ListGroup,
  ListGroupItem,
  // NavbarToggler,
  Nav,
  Navbar,
  NavItem,
  NavLink,
  Popover,
  PopoverBody,

} from 'reactstrap';
import bn from 'utils/bemnames';
import { LinkContainer } from "react-router-bootstrap";
import { Auth } from "aws-amplify";


const bem = bn.create('header');


class Header extends React.Component {
  state = {
    isOpenNotificationPopover: false,
    isNotificationConfirmed: false,
    isOpenUserCardPopover: false,
    userEmail: ""
  };

  toggleNotificationPopover = () => {
    this.setState({
      isOpenNotificationPopover: !this.state.isOpenNotificationPopover,
    });

    if (!this.state.isNotificationConfirmed) {
      this.setState({ isNotificationConfirmed: true });
    }
  };

  toggleUserCardPopover = () => {
    this.setState({
      isOpenUserCardPopover: !this.state.isOpenUserCardPopover,
    });
  };

  handleSidebarControlButton = event => {
    event.preventDefault();
    event.stopPropagation();

    document.querySelector('.cr-sidebar').classList.toggle('cr-sidebar--open');
  };

  async componentDidMount() {
    const user = await Auth.currentAuthenticatedUser();
    this.setState({
      userEmail: user.attributes.email
    });
  }
  

  render() {
    console.log(this.props.authenticator.isAuthenticated)
    
    return (
      <Navbar light expand className={bem.b('bg-white')}>
        <Nav navbar className="mr-2">
          <Button outline onClick={this.handleSidebarControlButton}>
            <MdClearAll size={25} />
          </Button>
        </Nav>
        <Nav navbar>
          <SearchInput />
        </Nav>

        <Nav navbar className={bem.e('nav-right')}>
        {this.props.authenticator.isAuthenticated ?
          <NavItem>
            <NavLink id="Popover2">
              <Avatar
                onClick={this.toggleUserCardPopover}
                className="can-click"
                src={me}
              />
            </NavLink>
            <Popover
              placement="bottom-end"
              isOpen={this.state.isOpenUserCardPopover}
              toggle={this.toggleUserCardPopover}
              target="Popover2"
              className="p-0 border-0"
              style={{ minWidth: 250 }}
            >
              <PopoverBody className="p-0 border-light">
                <UserCard
                  title=""
                  subtitle={this.state.userEmail}
                  text=""
                  avatar={me}
                  className="border-light"
                >
                  <ListGroup flush>
                    <ListGroupItem tag="button" action className="border-light" onClick={this.props.handleLogout}>
                      <MdExitToApp /> Signout
                    </ListGroupItem>
                  </ListGroup>
                </UserCard>
              </PopoverBody>
            </Popover>
          </NavItem>
          :
          <React.Fragment>
                  <LinkContainer to="/signup">
                    <NavItem><Button outline color="info">Signup</Button></NavItem>
                  </LinkContainer>
                  <LinkContainer to="/login">
                    <NavItem><Button outline color="success">Login</Button></NavItem>
                  </LinkContainer>
                  </React.Fragment>
          }
        </Nav>
        
      </Navbar>
    );
  }
}

export default Header;
