import React from 'react';
import { Route } from 'react-router-dom';

const LayoutRoute = ({ component: Component, layout: Layout, props: cProps, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <Layout {...props} {...cProps}>
        <Component {...props} {...cProps} />
      </Layout>
    )}
  />
);

export default LayoutRoute;
