import React from 'react'

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

const formatDateDayMonth = (rawDate) => {
    let dateVar = new Date(rawDate);
    let formattedDate = dateVar.getDate() + "/" + ("0" + (dateVar.getMonth() + 1)).split(-2);//+"/" +dateVar.getFullYear();
    return formattedDate;
}

const formatDateMonthYear = (rawDate) => {
    let dateVar = new Date(rawDate);
    let formattedDate = ("0" + (dateVar.getMonth() + 1)).split(-2) + "/" + dateVar.getFullYear();
    return formattedDate;
}

const dateDiffInDays = (a, b) => {
    // Discard the time and time-zone information.
    let dateVarA = new Date(a);
    let dateVarB = new Date(b);
    const utc1 = Date.UTC(dateVarA.getFullYear(), dateVarA.getMonth(), dateVarA.getDate());
    const utc2 = Date.UTC(dateVarB.getFullYear(), dateVarB.getMonth(), dateVarB.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

const dateDiffInMonths = (previous, current) => {
    // Discard the time and time-zone information.
    let previousDate = new Date(previous);
    let currentDate = new Date(current);

    if (currentDate.getFullYear() > previousDate.getFullYear()) {
        return Math.abs(11 - (previousDate.getMonth() - currentDate.getMonth()));
    }
    else {
        return Math.abs(currentDate.getMonth() - previousDate.getMonth());
    }
}

const formatDateLocal = (rawDate) => {
    let dateVar = new Date(rawDate);
    let month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    let formattedDate = ("0" + (dateVar.getDate())).slice(-2) + " " + month_names_short[dateVar.getMonth()] + " " + dateVar.getHours() + ":" + ("0" + (dateVar.getMinutes())).slice(-2);
    return formattedDate;
}

export const getDailyData = (transactions) => {
    let today = new Date(Date.now());
    let daysDiff = dateDiffInDays(transactions[transactions.length - 1].createdAt, today);

    var dailyChartData = [];
    dailyChartData = ({
        income: new Array(daysDiff + 1).fill(0),
        expense: new Array(daysDiff + 1).fill(0),
        groceries: new Array(daysDiff + 1).fill(0),
        food: new Array(daysDiff + 1).fill(0),
        other: new Array(daysDiff + 1).fill(0),
        rollingBalance: new Array(daysDiff + 1).fill(0),
        date: []
    })


    for (let i = 0; i <= daysDiff; i++) {
        let dateVariable = new Date();
        dateVariable.setDate((today.getDate() - i))

        dailyChartData.date.push(formatDateDayMonth(dateVariable))

    }

    //Merging transaction JSON into daily data Array @dailyChartData[]
    transactions.forEach(element => {
        let amount = parseFloat(element.amount) / 100;
        let daysDiff = dateDiffInDays(element.createdAt, today);

        if (amount > 0) {
            dailyChartData.income[daysDiff] += amount
        }
        else if (element.merchant.category_code === "5812") {
            dailyChartData.food[daysDiff] += Math.abs(amount)
            dailyChartData.expense[daysDiff] += Math.abs(amount)
        }
        else if (element.merchant.category_code === "5411") {
            dailyChartData.groceries[daysDiff] += Math.abs(amount)
            dailyChartData.expense[daysDiff] += Math.abs(amount)
        }
        else {
            dailyChartData.other[daysDiff] += Math.abs(amount)
            dailyChartData.expense[daysDiff] += Math.abs(amount)
        }
    });

    dailyChartData.rollingBalance[daysDiff] = dailyChartData.income[daysDiff] + dailyChartData.expense[daysDiff]
    for (var i = daysDiff - 1; i >= 0; i--) {
        dailyChartData.rollingBalance[i] = dailyChartData.income[i] - dailyChartData.expense[i] + dailyChartData.rollingBalance[i + 1]
    }

    return dailyChartData;

}


export const getMonthlyData = (transactions) => {
    var monthlyChartData = [];
    let today = new Date(Date.now());

    monthlyChartData = ({
        income: new Array(12).fill(0),
        expense: new Array(12).fill(0),
        groceries: new Array(12).fill(0),
        food: new Array(12).fill(0),
        other: new Array(12).fill(0),
        date: []
    })

    for (let i = 0; i < 12; i++) {
        let dateVariable = new Date();
        dateVariable.setMonth((today.getMonth() - i))
        monthlyChartData.date.push(formatDateMonthYear(dateVariable));
    }

    //Merging transaction JSON into daily data Array @dailyChartData[]
    transactions.forEach(element => {
        let amount = parseFloat(element.amount) / 100;
        let monthsDiff = dateDiffInMonths(element.createdAt, today);
        if (amount > 0) {
            monthlyChartData.income[monthsDiff] = parseFloat(monthlyChartData.income[monthsDiff]) + amount
        }
        else if (element.merchant.category_code === "5812") {
            monthlyChartData.food[monthsDiff] = parseFloat(monthlyChartData.food[monthsDiff]) + Math.abs(amount)
            monthlyChartData.expense[monthsDiff] = parseFloat(monthlyChartData.expense[monthsDiff]) + Math.abs(amount)
        }
        else if (element.merchant.category_code === "5411") {
            monthlyChartData.groceries[monthsDiff] = parseFloat(monthlyChartData.groceries[monthsDiff]) + Math.abs(amount)
            monthlyChartData.expense[monthsDiff] = parseFloat(monthlyChartData.expense[monthsDiff]) + Math.abs(amount)
        }
        else {
            monthlyChartData.other[monthsDiff] = parseFloat(monthlyChartData.other[monthsDiff]) + Math.abs(amount)
            monthlyChartData.expense[monthsDiff] = parseFloat(monthlyChartData.expense[monthsDiff]) + Math.abs(amount)
        }
    });

    return monthlyChartData;
}


export const getTransactionTable = (transactions) => {
    var tableContent = [];
    transactions.forEach(element => {
        let amount = parseFloat(element.amount) / 100;
        if (amount < 0) {
            let children = [];
            children.push(<td>{`1`}</td>)
            children.push(<td>{element.merchant.name}</td>)
            children.push(<td>{formatDateLocal(element.createdAt)}</td>)
            children.push(<td>{`R ${Math.abs(amount).toFixed(2)}`}</td>)
            if (amount < 0) {
                tableContent.push(<tr>
                    {
                        children
                    }
                </tr>)
            }
        }
    });

    return tableContent;
}