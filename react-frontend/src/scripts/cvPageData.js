

export const getSkills = () => {
    return ([{
        color: 'primary',
        rating: '90',
        name: 'Problem Solving'  
    },
    {
        color: 'secondary',
        rating: '87',
        name: 'Python'  
    },
    {
        color: 'success',
        rating: '81',
        name: 'Java'  
    },
    {
        color: 'info',
        rating: '75',
        name: 'NodeJS'  
    },
    {
        color: 'danger',
        rating: '75',
        name: 'React'  
    }]);
}


export const getExperience = () => {
    return ([{
        date: '            2018-present',
        title: 'Cloud Support Engineer',
        subtitle: 'Amazon Web Services',
        description: '',
        icon: 'work'
    },
    {
        date: '2017-2018            ' ,
        title: 'Software Development Engineer',
        subtitle: 'ETA Operations',
        description: 'I then moved my way up to Software Developer working with the raw data sets and creating algorythms to process the data into useable information. Python.',
        icon: 'work' 
    },
    {
        date: '            2016-2017',
        title: 'Project Engineer',
        subtitle: 'ETA Operations',
        description: 'While completing my Masters I was also working as Project Engineer for an Electricity Efficiency company. My responsibilites included on-site visits, practical metering and creating Reports for energy use investigations.',
        icon: 'work' 
    },
    {
        date: '2016-2018            ',
        title: 'Masters In Electrical and Electronic Engineering',
        subtitle: 'North West Universtiy',
        description: 'Completed my masters investigating electricity usage and a reporting structure to find locate efficiencies. Worked with data handling in Python.',
        icon: 'school' 
    },
    {
        date: '            2012-2016',
        title: 'Honors in Electrical and Electronic Engineering',
        subtitle: 'Stellenbosch Universtiy',
        description: 'Learnt the essentials of Java, C, Assembly as well as circuit design.',
        icon: 'school' 
    }]);
}