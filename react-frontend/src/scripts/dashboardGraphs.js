import ChartJS from 'chart.js';


const getRandomInt = () => {
  return Math.floor(Math.random() * (100 - 20) + 20);
};

const skeletonAwait = (dataObject, wantedKey, wantedItems) => {
  if (dataObject != null) {
    return dataObject[wantedKey].slice(0, parseInt(wantedItems)).reverse();
  } else {
    let dummyData = [];
    for (var i = 0; i < wantedItems; i++) {
      dummyData.push(getRandomInt());
    }
    return dummyData;
  }

}



export const getExpensesGraph = (dataObject) => {
  var expensesGraph = {
    labels: skeletonAwait(dataObject, "date", 30),
    datasets: [
      {
        label: 'Food',
        borderColor: '#6a82fb',
        backgroundColor: '#6a82fb',
        data: skeletonAwait(dataObject, "food", 30),
      },
      {
        label: 'Groceries',
        borderColor: '#5CF5FC',
        backgroundColor: '#5CF5FC',
        data: skeletonAwait(dataObject, "groceries", 30),
      },
      {
        label: 'Other',
        borderColor: '#fc5c7d',
        backgroundColor: '#fc5c7d',
        data: skeletonAwait(dataObject, "other", 30),
      },


    ],
  }
  return expensesGraph;
}

export const getPieOverviewGraph = (dataObject) => {
  var balanceGraph = {
    labels: (dataObject == null) ? ["14/04", "13/04", "12/04", "11/04", "10/04", "9/04", "8/04", "7/04", "6/04", "5/04", "4/04", "3/04", "2/04", "1/04", "31/03", "30/03", "29/03", "28/03", "27/03", "26/03", "25/03", "24/03", "23/03", "22/03", "21/03", "20/03", "19/03", "18/03", "17/03", "16/03"] : dataObject.date.slice(0, 30),
    datasets: [
      {
        backgroundColor: ['#6a82fb', '#5CF5FC', '#fc5c7d'],
        data: [skeletonAwait(dataObject, "food", 1), skeletonAwait(dataObject, "groceries", 1), skeletonAwait(dataObject, "other", 1)],
      },
    ],
    labels: ['Food', 'Groceries', 'Other'],
  }
  return balanceGraph;
}

export const getYearlyOverviewGraph = (dataObject) => {
  var balanceGraph = {
    labels: (dataObject == null) ? ["14/04", "13/04", "12/04", "11/04", "10/04", "9/04", "8/04", "7/04", "6/04", "5/04", "4/04", "3/04", "2/04", "1/04", "31/03", "30/03", "29/03", "28/03", "27/03", "26/03", "25/03", "24/03", "23/03", "22/03", "21/03", "20/03", "19/03", "18/03", "17/03", "16/03"] : dataObject.date.slice(0, 30),
    datasets: [
      {
        label: 'Food',
        borderColor: '#6a82fb',
        backgroundColor: '#6a82fb',
        data: skeletonAwait(dataObject, "food", 12),
      },
      {
        label: 'Groceries',
        borderColor: '#5CF5FC',
        backgroundColor: '#5CF5FC',
        data: skeletonAwait(dataObject, "groceries", 12),
      },
      {
        label: 'Other',
        borderColor: '#fc5c7d',
        backgroundColor: '#fc5c7d',
        data: skeletonAwait(dataObject, "other", 12),
      }
    ],
  }
  return balanceGraph;
}

export const getRollingBalanceGraph = (dataObject) => canvas => {
  const ctx = canvas.getContext('2d');
  let gradient = ctx.createLinearGradient(0, 0, 0, 240);
  gradient.addColorStop(
    0,
    ChartJS.helpers
      .color('#00c9ff')
      .alpha(1)
      .rgbString()
  );
  gradient.addColorStop(
    1,
    ChartJS.helpers
      .color('#00c9ff')
      .alpha(0.2)
      .rgbString()
  );
  var balanceGraph = {
    labels: (dataObject == null) ? ["14/04", "13/04", "12/04", "11/04", "10/04", "9/04", "8/04", "7/04", "6/04", "5/04", "4/04", "3/04", "2/04", "1/04", "31/03", "30/03", "29/03", "28/03", "27/03", "26/03", "25/03", "24/03", "23/03", "22/03", "21/03", "20/03", "19/03", "18/03", "17/03", "16/03"] : dataObject.date.slice(0, 60),
    datasets: [
      {
        label: 'Rolling Balance',
        backgroundColor: gradient,
        data: skeletonAwait(dataObject, "rollingBalance", 60),
      },
    ],
  }
  return balanceGraph;
}
